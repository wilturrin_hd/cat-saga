import { combineReducers } from 'redux';
import { default as DataStore } from './dataReducers';

const appReducer = combineReducers({
    DataStore
});

const rootReducer = (state:any, action:any) => {
    return appReducer(state, action);
};
export default rootReducer;
