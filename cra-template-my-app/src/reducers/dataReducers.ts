import {
    FETCH_DATA_FAILED,
    FETCH_DATA_RECEIVED,
    FETCH_DATA
} from '../actions/types';

import _ from 'lodash';

const initialState = {
    authenticated: true,
    data: [],
    hasError: false,
    isLoading: false
};

export default function dataReducer (state:any = initialState, action:any) {
    let newState = state;
    switch (action.type) {
        case FETCH_DATA_FAILED:
            newState = Object.assign({}, state, {hasError: true, isLoading: false});
            return newState;
        case FETCH_DATA_RECEIVED:
            let newData:Array<any> = [];
            if(!_.isEmpty(action.response.data)){
                const data = action.response.data;
                newData = data.map((cat:any)=>{
                    return {
                        name: 'Cat - ' + cat.id,
                        url: cat.url
                    }
                })
            }
            newState = Object.assign({}, state, {hasError: false, data: newData, isLoading: false});
            return newState;
        case FETCH_DATA:
            newState = Object.assign({}, state, {hasError: false, isLoading: true});
            return newState;
        default:
            return state;
    }
}