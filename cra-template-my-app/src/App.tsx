import React, {Component} from 'react';
import './App.css';
import DataList from "./containers/DataList/DataList";
import {connect} from "react-redux";
import {bindActionCreators} from 'redux';

import {fetchData} from "./actions/dataActions";

import _ from 'lodash';

type AppProps = {
    actions: {
        fetchData: () => {}
    },
    DataStore: any
}

class App extends Component<AppProps, {}> {

    /* private values */
    private dataList:any;

    /* React Lifecycle */
    // shouldComponentUpdate is similar to ngOnChanges
    shouldComponentUpdate(nextProps: Readonly<AppProps>, nextState: Readonly<{ DataStore: {} }>, nextContext: any): boolean {
        if (!_.isEqual(nextProps?.DataStore, this.props?.DataStore)) {
            // set list values, always clone the values
            this.dataList = _.cloneDeep(nextProps.DataStore.data);
        }
        return true;
    }

    fetchData = () => {
        this.props.actions.fetchData();
    }

    render = () => {
        return (
            <div className="App">
                <header className="App-header">
                    <DataList
                        fetchData={this.fetchData}
                        dataList={this.dataList}
                        isLoading={this.props.DataStore.isLoading}
                    >
                    </DataList>
                </header>
            </div>
        );
    }

}

// Redux.connect can be used to get data from the Redux store snf/or dispatch actions on the Redux store.
// It makes the container component into a higher-order component returned by connect().
//
// source: https://blog.logrocket.com/react-redux-connect-when-and-how-to-use-it-f2a1edab2013/

// Associate only the Stores you are interested in. Each higher level component would need a different store makeup.
const mapStateToProps = (state: any) => {
    return {
        DataStore: state.DataStore
    };
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        actions: bindActionCreators({
            fetchData
        }, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

