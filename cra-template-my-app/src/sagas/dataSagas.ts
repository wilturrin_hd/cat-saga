import { put, takeLatest, call, delay } from 'redux-saga/effects';
import apiHelper from '../api/apiHelper';

import {
    FETCH_DATA_FAILED,
    FETCH_DATA_RECEIVED,
    FETCH_DATA
} from '../actions/types';

function* getData(action:any){
    try {
        const response = yield call(apiHelper.fetchDataApi, action);
        yield delay(500)
        yield put({ type: FETCH_DATA_RECEIVED, response });
    }
    catch (error) {
        yield put({ type: FETCH_DATA_FAILED, error });
    }
}

export function* watchFetchData() {
    yield takeLatest(FETCH_DATA, getData);
}
