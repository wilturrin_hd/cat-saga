import { all, fork } from 'redux-saga/effects';
import * as dataSagas from './dataSagas';

// import watchers from other files
export default function* rootSaga() {
    yield all([
        ...Object.values(dataSagas),
    ].map(fork));
}