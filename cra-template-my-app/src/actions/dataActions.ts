import {
    FETCH_DATA,
} from '../actions/types';

export const fetchData = () => ({
    type: FETCH_DATA
});