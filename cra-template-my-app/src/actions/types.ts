export const FETCH_DATA_FAILED = 'FETCH_DATA_FAILED';
export const FETCH_DATA_RECEIVED = 'FETCH_DATA_RECEIVED';
export const FETCH_DATA = 'FETCH_DATA';