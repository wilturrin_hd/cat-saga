import React, {Component} from "react";
import _ from 'lodash';

import './DataList.css';

interface DataListProps {
    fetchData: Function,
    dataList?: any,
    data?: any,
    isLoading: boolean
}

interface DataListState {
}

class DataList extends Component<DataListProps, DataListState> {

    isLoading = (): boolean => {
        return this.props.isLoading;
    }

    showDataList = (): boolean => {
        return !_.isEmpty(this.props.dataList)
    }

    onFetchData = () => {
        this.props.fetchData();
    }

    componentDidMount() {
        if(_.isEmpty(this.props.dataList)){
            this.onFetchData();
        }
    }

    renderLoading = () => {
        return (<div>Loading...</div>);
    }

    renderList = () => {
        return (
            this.props.dataList.map((cat: any) => {
                return (
                    <figure className={'picture-holder'} key={cat.name}>
                        <img src={cat.url} alt={cat.name}/>
                        <figcaption>{cat.name}</figcaption>
                    </figure>)
            })
        );
    }

    render = () => {
        return (
            <>
                <button onClick={this.onFetchData}>Click for a new Cat</button>
                {this.isLoading() && this.renderLoading()}
                {!this.isLoading() &&this.showDataList() && this.renderList()}
            </>
        );
    }
}

export default DataList;




