const axios = require('axios').default;

const apiHelper = {
    fetchDataApi: (data:any) => {
        let url = 'https://api.thecatapi.com/v1/images/search';
        return axios({
            method: 'get',
            url: url,
            crossdomain: true
        });
    }
};

export default apiHelper;